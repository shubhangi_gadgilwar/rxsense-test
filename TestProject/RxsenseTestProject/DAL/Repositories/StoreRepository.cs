﻿using DAL.Interfaces;
using Model;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
    public class StoreRepository : IStoreRepository
    {
        public List<StoreModel> GetStoreDetails(int cityId)
        {
            RxsenseTestEntities rxsenseTestEntities = new RxsenseTestEntities();
            List<StoreModel> lstStoreModel = new List<StoreModel>();

            lstStoreModel = (from sto in rxsenseTestEntities.Stores
                             join ci in rxsenseTestEntities.Cities on sto.CityID equals ci.CityId
                             where (ci.CityId == cityId)
                             select new StoreModel
                             {
                                 CityID = ci.CityId,
                                 StoreName = sto.StoreName,
                                 StoreId = sto.StoreId,
                                 CityName = ci.CityName
                             }).ToList();

            return lstStoreModel;
        }

        public bool CheckStoreAvailableOrNot(int storeId)
        {
            RxsenseTestEntities rxsenseTestEntities = new RxsenseTestEntities();

            var storeModel = (from sto in rxsenseTestEntities.Stores
                              join ci in rxsenseTestEntities.Cities on sto.CityID equals ci.CityId
                              where (sto.StoreId == storeId)
                              select new StoreModel
                              {
                                  CityID = ci.CityId,
                                  StoreName = sto.StoreName,
                                  StoreId = sto.StoreId,
                                  CityName = ci.CityName
                              }).FirstOrDefault();

            if (storeModel != null)
                return true;

            return false;
        }
    }
}
