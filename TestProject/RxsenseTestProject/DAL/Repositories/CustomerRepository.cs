﻿using DAL.Interfaces;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        public List<CustomerModel> GetAllCustomerDetails()
        {
            RxsenseTestEntities rxsenseTestEntities = new RxsenseTestEntities();
            List<CustomerModel> lstCustomerModel = new List<CustomerModel>();

            lstCustomerModel = (from cus in rxsenseTestEntities.CustomerDetails
                                join sto in rxsenseTestEntities.Stores on cus.StoreId equals sto.StoreId
                                join ci in rxsenseTestEntities.Cities on sto.CityID equals ci.CityId
                                where cus.IsActive == true
                                select new CustomerModel
                                {
                                    CustomerId = cus.CustomerId,
                                    FullName = cus.FirstName + " " + cus.LastName,
                                    CustomerAddress = cus.CustomerAddress,
                                    ContactNumber = cus.ContactNumber,
                                    City = ci.CityName,
                                    Store = sto.StoreName,
                                    FirstName = cus.FirstName,
                                    LastName = cus.LastName,
                                    IsActive = cus.IsActive,
                                    CreatedDate = cus.CreatedDate,
                                    UpdatedDate = cus.UpdatedDate,
                                    StoreId=sto.StoreId

                                }).ToList();

            return lstCustomerModel;
        }

        public int AddCustomer(CustomerDetail customerDetail)
        {
            int result = 0;
            RxsenseTestEntities rxsenseTestEntities = new RxsenseTestEntities();
            rxsenseTestEntities.CustomerDetails.Add(customerDetail);
            result = rxsenseTestEntities.SaveChanges();
            return result;
        }

        public List<CustomerModel> SearchCustomerDetails(int? customerID, string name)
        {
            RxsenseTestEntities rxsenseTestEntities = new RxsenseTestEntities();
            List<CustomerModel> lstcustomerModel = new List<CustomerModel>();

            customerID = customerID ?? 0;
            name = name?.Trim() ?? "";

            lstcustomerModel = (from cus in rxsenseTestEntities.CustomerDetails
                                join sto in rxsenseTestEntities.Stores on cus.StoreId equals sto.StoreId
                                join ci in rxsenseTestEntities.Cities on sto.CityID equals ci.CityId
                                where cus.IsActive == true && ((customerID > 0 && cus.CustomerId == customerID)) || (cus.FirstName.StartsWith(name))
                                select new CustomerModel
                                {
                                    CustomerId = cus.CustomerId,
                                    FullName = cus.FirstName + " " + cus.LastName,
                                    CustomerAddress = cus.CustomerAddress,
                                    ContactNumber = cus.ContactNumber,
                                    City = ci.CityName,
                                    Store = sto.StoreName,
                                    FirstName = cus.FirstName,
                                    LastName = cus.LastName,
                                    IsActive = cus.IsActive,
                                    CreatedDate = cus.CreatedDate,
                                    UpdatedDate = cus.UpdatedDate,
                                    StoreId=sto.StoreId

                                }).ToList();

            return lstcustomerModel;
        }

        public List<CustomerModel> GetCustomerDetailsByStore(int storeID)
        {
            RxsenseTestEntities rxsenseTestEntities = new RxsenseTestEntities();
            List<CustomerModel> lstcustomerModel = new List<CustomerModel>();

            lstcustomerModel = (from cus in rxsenseTestEntities.CustomerDetails
                                join sto in rxsenseTestEntities.Stores on cus.StoreId equals sto.StoreId
                                join ci in rxsenseTestEntities.Cities on sto.CityID equals ci.CityId
                                where cus.IsActive == true && (cus.StoreId == storeID)
                                select new CustomerModel
                                {
                                    CustomerId = cus.CustomerId,
                                    FullName = cus.FirstName + " " + cus.LastName,
                                    CustomerAddress = cus.CustomerAddress,
                                    ContactNumber = cus.ContactNumber,
                                    City = ci.CityName,
                                    Store = sto.StoreName,
                                    FirstName = cus.FirstName,
                                    LastName = cus.LastName,
                                    IsActive = cus.IsActive,
                                    CreatedDate = cus.CreatedDate,
                                    UpdatedDate = cus.UpdatedDate,
                                    StoreId = sto.StoreId

                                }).ToList();

            return lstcustomerModel;
        }

        public int UpdateCustomer(CustomerDetail updateCustomerDetail)
        {
            int result = 0;
            RxsenseTestEntities rxsenseTestEntities = new RxsenseTestEntities();

            CustomerDetail customerDetail = new CustomerDetail();

            customerDetail = (from cus in rxsenseTestEntities.CustomerDetails
                               join sto in rxsenseTestEntities.Stores
                              on cus.StoreId equals sto.StoreId
                               where cus.CustomerId == updateCustomerDetail.CustomerId && cus.IsActive==true
                               select cus).FirstOrDefault();

            if (customerDetail != null)
            {
                customerDetail.StoreId = updateCustomerDetail.StoreId;
                customerDetail.FirstName = updateCustomerDetail.FirstName;
                customerDetail.LastName = updateCustomerDetail.LastName;
                customerDetail.CustomerAddress = updateCustomerDetail.CustomerAddress;
                customerDetail.ContactNumber = updateCustomerDetail.ContactNumber;
                customerDetail.UpdatedDate = DateTime.Now;
                result = rxsenseTestEntities.SaveChanges();
            }

            return result;
        }

        public int DeleteCustomer(int customerID)
        {
            int result = 0;
            RxsenseTestEntities rxsenseTestEntities = new RxsenseTestEntities();

            CustomerDetail customerDetail = new CustomerDetail();

            customerDetail = (from cus in rxsenseTestEntities.CustomerDetails
                              join sto in rxsenseTestEntities.Stores
                             on cus.StoreId equals sto.StoreId
                              where cus.CustomerId == customerID && cus.IsActive == true
                              select cus).FirstOrDefault();

            if (customerDetail != null)
            {              
                customerDetail.IsActive = false;
                customerDetail.UpdatedDate = DateTime.Now;
                result = rxsenseTestEntities.SaveChanges();
            }

            return result;
        }
    }
}
