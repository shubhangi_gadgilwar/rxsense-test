﻿using Model;
using System.Collections.Generic;

namespace DAL.Interfaces
{
    public interface IStoreRepository
    {
        List<StoreModel> GetStoreDetails(int cityId);
        bool CheckStoreAvailableOrNot(int storeId);
    }
}
