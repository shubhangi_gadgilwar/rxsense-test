﻿using Model;
using System.Collections.Generic;

namespace DAL.Interfaces
{
    public interface ICustomerRepository
    {
        List<CustomerModel> GetAllCustomerDetails();
        int AddCustomer(CustomerDetail customerDetail);
        List<CustomerModel> SearchCustomerDetails(int? customerID, string name);
        List<CustomerModel> GetCustomerDetailsByStore(int storeID);
        int UpdateCustomer(CustomerDetail updateCustomerDetail);
        int DeleteCustomer(int customerID);
    }
}
