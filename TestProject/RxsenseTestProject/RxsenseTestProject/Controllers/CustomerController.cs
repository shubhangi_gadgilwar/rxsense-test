﻿using BO.BO;
using BO.Interfaces;
using Model;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RxsenseTestProject.Controllers
{
    public class CustomerController : ApiController
    {
        public CustomerController()
        {
        }

        [HttpGet]
        [Route("get-customerdetails")]
        public HttpResponseMessage GetAllCustomerDetails()
        {
            try
            {
                List<CustomerModel> lstCustomerModel = new List<CustomerModel>();
                ICustomerBO custBO = new CustomerBO();
                lstCustomerModel = custBO.GetAllCustomerDetails();
                if (lstCustomerModel.Count > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.Found, lstCustomerModel);
                }
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Record not available");
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No Data found");
            }
        }

        [HttpGet]
        [Route("searchcustomerdetails")]
        public HttpResponseMessage SearchCustomerDetails(int? customerId, string name)
        {
            try
            {
                List<CustomerModel> lstCustomerModel = new List<CustomerModel>();
                ICustomerBO custBO = new CustomerBO();
                lstCustomerModel = custBO.SearchCustomerDetails(customerId, name);
                if (lstCustomerModel.Count > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.Found, lstCustomerModel);
                }
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Record not available");
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No Data found");
            }
        }


        [HttpPost]
        [Route("addcustomerdetails")]
        public HttpResponseMessage AddCustomerDetails(AddCustomerModel customerModel)
        {
            if (ModelState.IsValid)
            {
                string message = string.Empty;
                int result = 0;
                ICustomerBO custBO = new CustomerBO();
                IStoreBO storeBo = new StoreBO();
                if (storeBo.CheckStoreAvailableOrNot(customerModel.StoreId))
                {
                    result = custBO.AddCustomer(customerModel);
                    if (result > 0)
                    {
                        message = "Record inserted successfully.";
                        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, message);
                        return response;
                    }
                    else
                    {
                        message = "Record not inserted.";
                        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest, message);
                        return response;
                    }
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Store Not Available");
                }
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }


        [HttpGet]
        [Route("customerdetails")]
        public HttpResponseMessage GetCustomerDetailsByStore(int storeID)
        {
            try
            {
                List<CustomerModel> lstCustomerModel = new List<CustomerModel>();
                ICustomerBO custBO = new CustomerBO();

                lstCustomerModel = custBO.GetCustomerDetailsByStore(storeID);
                if (lstCustomerModel.Count > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.Found, lstCustomerModel);
                }
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Record not available");
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No Data found");
            }
        }

        [HttpPut]
        [Route("update")]
        public HttpResponseMessage UpdateCustomerDetails(AddCustomerModel customerModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string message = string.Empty;
                    int result = 0;
                    ICustomerBO custBO = new CustomerBO();
                    IStoreBO storeBo = new StoreBO();
                    if (storeBo.CheckStoreAvailableOrNot(customerModel.StoreId))
                    {
                        result = custBO.UpdateCustomer(customerModel);
                        if (result > 0)
                        {
                            message = "Record updated successfully.";
                            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, message);
                            return response;
                        }
                        else
                        {
                            message = "Record not updated.";
                            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.NotModified, message);
                            return response;
                        }
                    }
                    else
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Store Not Available");
                    }
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        [HttpPut]
        [Route("delete")]
        public HttpResponseMessage DeleteCustomer(int customerID)
        {
            try
            {

                string message = string.Empty;
                int result = 0;
                ICustomerBO custBO = new CustomerBO();
                result = custBO.DeleteCustomer(customerID);
                if (result > 0)
                {
                    message = "Record deleted successfully.";
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, message);
                    return response;
                }
                else
                {
                    message = "Record not deleted.";
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.NotModified, message);
                    return response;
                }
            }
            catch (Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }
    }
}
