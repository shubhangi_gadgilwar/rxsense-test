﻿using BO.BO;
using BO.Interfaces;
using Model;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RxsenseTestProject.Controllers
{
    public class StoreController : ApiController
    {
        [HttpGet]
        [Route("getstoredetails")]
        public HttpResponseMessage GetStoreDetails(int cityId)
        {
            try
            {
                List<StoreModel> lstStoreModel = new List<StoreModel>();
                IStoreBO storeBo = new StoreBO();
                lstStoreModel = storeBo.GetStoreDetails(cityId);
                if (lstStoreModel.Count > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.Found, lstStoreModel);
                }
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound,"Record not available");
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No Data found");
            }
        }

    }
}
