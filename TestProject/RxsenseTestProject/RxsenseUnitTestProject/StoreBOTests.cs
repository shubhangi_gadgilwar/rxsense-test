﻿using BO.BO;
using BO.Interfaces;
using DAL.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xunit;

namespace RxsenseUnitTestProject
{
    /// <summary>
    /// Summary description for StoreBOTests
    /// </summary>
    [TestClass]
    [TestCategory("StoreBO")]
    public class StoreBOTests
    {
        private readonly Mock<IStoreBO> mockStoreBO;
        private readonly Mock<IStoreRepository> mockStoreRepository;
        private readonly StoreBO storeBO;
        public StoreBOTests()
        {
            this.mockStoreRepository = new Mock<IStoreRepository>();
            this.mockStoreBO = new Mock<IStoreBO>();
            this.storeBO = new StoreBO(this.mockStoreRepository.Object);
        }


        [TestMethod]
        public void HasStoreAvailableOrNot_RetursTrue()
        {
            int storeId = 1;
            this.mockStoreBO
                .Setup(bo => bo.CheckStoreAvailableOrNot(It.IsAny<int>()))
                .Returns(true);
            this.mockStoreRepository
                .Setup(db => db.CheckStoreAvailableOrNot(It.IsAny<int>())).Returns(true);

            bool isAvailable = this.storeBO.CheckStoreAvailableOrNot(storeId);

            isAvailable.Equals(true == isAvailable);
        }

        [TestMethod]
        public void HasStoreAvailableOrNot_RetursFalse()
        {
            int storeId = 12;
            this.mockStoreBO
                .Setup(bo => bo.CheckStoreAvailableOrNot(It.IsAny<int>()))
                .Returns(true);
            this.mockStoreRepository
                .Setup(db => db.CheckStoreAvailableOrNot(It.IsAny<int>())).Returns(false);

            bool isAvailable = this.storeBO.CheckStoreAvailableOrNot(storeId);

            isAvailable.Equals(false == isAvailable);
        }
    }
}