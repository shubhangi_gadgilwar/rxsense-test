﻿using BO.BO;
using BO.Interfaces;
using DAL.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace RxsenseUnitTestProject
{
    /// <summary>
    /// Summary description for CustomerBOTests
    /// </summary>
    [TestClass]
    [TestCategory("CustomerBO")]
    public class CustomerBOTests
    {
        private readonly Mock<ICustomerBO> mockCustomerBO;
        private readonly Mock<ICustomerRepository> mockCustomerRepository;
        private readonly CustomerBO customerBO;

        public CustomerBOTests()
        {

            this.mockCustomerRepository = new Mock<ICustomerRepository>();
            this.mockCustomerBO = new Mock<ICustomerBO>();
            this.customerBO = new CustomerBO(this.mockCustomerRepository.Object);
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
             
        [TestMethod]
        public void TestMethod1()
        {
            //
            // TODO: Add test logic here
            //
        }
    }
}
