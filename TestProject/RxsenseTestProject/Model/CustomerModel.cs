﻿using System;

namespace Model
{
    public  class CustomerModel
    {
        public long CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string ContactNumber { get; set; }
        public int StoreId { get; set; }
        public string CustomerAddress { get; set; }
        public string City { get; set; }
        public string Store { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

    public class AddCustomerModel
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }      
        public string ContactNumber { get; set; }
        public int StoreId { get; set; }
        public string CustomerAddress { get; set; }    
    }
}
