﻿namespace Model
{
    public class StoreModel
    {
        public int StoreId { get; set; }
        public string StoreName { get; set; }
        public int CityID { get; set; }
        public string CityName { get; set; }
    }

}
