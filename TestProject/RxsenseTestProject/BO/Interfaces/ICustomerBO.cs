﻿using Model;
using System.Collections.Generic;

namespace BO.Interfaces
{
    public interface ICustomerBO
    {
        List<CustomerModel> GetAllCustomerDetails();
        int AddCustomer(AddCustomerModel customerModel);
        List<CustomerModel> SearchCustomerDetails(int? customerID, string name);
        List<CustomerModel> GetCustomerDetailsByStore(int storeID);
        int UpdateCustomer(AddCustomerModel customerModel);
        int DeleteCustomer(int customerID);
    }
}
