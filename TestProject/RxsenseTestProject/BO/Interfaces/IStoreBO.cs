﻿using Model;
using System.Collections.Generic;

namespace BO.Interfaces
{
    public interface IStoreBO
    {
        List<StoreModel> GetStoreDetails(int cityId );
        bool CheckStoreAvailableOrNot(int storeId);
    }
}
