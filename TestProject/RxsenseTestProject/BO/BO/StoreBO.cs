﻿using BO.Interfaces;
using DAL.Interfaces;
using DAL.Repositories;
using Model;
using System;
using System.Collections.Generic;

namespace BO.BO
{
    public class StoreBO : IStoreBO
    {
        private readonly IStoreRepository storeRepository;

        public StoreBO() : this(new StoreRepository())
        {
        }
        public StoreBO(IStoreRepository storeRepository)
        {
            this.storeRepository = storeRepository ?? throw new ArgumentNullException(nameof(storeRepository));
        }

        public List<StoreModel> GetStoreDetails(int cityId)
        {
            List<StoreModel> lstStoreModel = new List<StoreModel>();
            lstStoreModel = storeRepository.GetStoreDetails(cityId);
            return lstStoreModel;
        }

        public bool CheckStoreAvailableOrNot(int storeId)
        {
            return storeRepository.CheckStoreAvailableOrNot(storeId);
        }
    }
}
