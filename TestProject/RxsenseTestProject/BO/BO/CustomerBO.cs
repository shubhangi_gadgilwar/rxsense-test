﻿using BO.Interfaces;
using DAL;
using DAL.Interfaces;
using DAL.Repositories;
using Model;
using System;
using System.Collections.Generic;

namespace BO.BO
{
    public class CustomerBO : ICustomerBO
    {
        private readonly ICustomerRepository customerRepository;

        public CustomerBO() : this(new CustomerRepository())
        {
        }
        public CustomerBO(ICustomerRepository customerRepository)
        {
            this.customerRepository = customerRepository ?? throw new ArgumentNullException(nameof(customerRepository));
        }

        public List<CustomerModel> GetAllCustomerDetails()
        {
            List<CustomerModel> lstCustomerModel = new List<CustomerModel>();
            lstCustomerModel = customerRepository.GetAllCustomerDetails();
            return lstCustomerModel;
        }

        public int AddCustomer(AddCustomerModel customerModel)
        {
            int result = 0;
            CustomerDetail customerDetail = new CustomerDetail();
            customerDetail.FirstName = customerModel.FirstName;
            customerDetail.LastName = customerModel.LastName;
            customerDetail.StoreId = customerModel.StoreId;
            customerDetail.CustomerAddress = customerModel.CustomerAddress;
            customerDetail.ContactNumber = customerModel.ContactNumber;
            customerDetail.IsActive = true;
            customerDetail.UpdatedDate = DateTime.Now;
            customerDetail.CreatedDate = DateTime.Now;
            result = customerRepository.AddCustomer(customerDetail);
            return result;
        }

        public List<CustomerModel> SearchCustomerDetails(int? customerID, string name)
        {
            List<CustomerModel> lstCustomerModel = new List<CustomerModel>();
            lstCustomerModel = customerRepository.SearchCustomerDetails(customerID, name);
            return lstCustomerModel;
        }

        public List<CustomerModel> GetCustomerDetailsByStore(int storeID)
        {
            List<CustomerModel> lstCustomerModel = new List<CustomerModel>();
            lstCustomerModel = customerRepository.GetCustomerDetailsByStore(storeID);
            return lstCustomerModel;
        }

        public int UpdateCustomer(AddCustomerModel customerModel)
        {
            int result = 0;
            CustomerDetail customerDetail = new CustomerDetail();
            customerDetail.FirstName = customerModel.FirstName;
            customerDetail.LastName = customerModel.LastName;
            customerDetail.StoreId = customerModel.StoreId;        
            customerDetail.ContactNumber = customerModel.ContactNumber;
            customerDetail.CustomerAddress = customerModel.CustomerAddress;
            customerDetail.CustomerId = customerModel.CustomerId;
            result = customerRepository.UpdateCustomer(customerDetail);
            return result;
        }

        public int DeleteCustomer(int customerID)
        {
            return customerRepository.DeleteCustomer(customerID);
        }
    }
}
